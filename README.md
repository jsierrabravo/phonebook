## Dependencies

* [pipenv](https://pipenv.pypa.io/en/latest/install/#installing-pipenv)


## Installation

1. `git clone git@gitlab.com:jsierrabravo/phonebook.git`

Everytime you pull the changes from this repository, you must execute the following commands:

2. `pipenv install`
3. `pipenv shell`
4. `python manage.py migrate`

Optionally, if you need to set up your IDE, you can use the command `which python` to get the environment's path.  

## Running the project

The following commands must be executed in different terminals in order to run the project properly:

5. `python manage.py runserver`
6. `celery -A phonebook worker -l info`
7. `celery -A phonebook beat -l info`
