from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import ContactList, Contact
from .forms import CreateNewList, CreateNewContact
from django.contrib.auth.decorators import login_required
from .tasks import send_email

# Create your views here.
login_url = '/login/'


def home(response):
    return render(response, 'home.html')


@login_required(login_url=login_url)
def contact_groups(response):
    groups = response.user.contactlist.all()
    context = {
        'groups': groups,
    }
    return render(response, 'contact_groups.html', context)


@login_required(login_url=login_url)
def contacts(response):
    # TODO: show only logged user's contacts
    all_contacts = Contact.objects.all()
    context = {
        'contacts': all_contacts,
    }
    return render(response, 'all_contacts.html', context)


@login_required(login_url=login_url)
def contact_list(response, id):
    # TODO: show list only to the owner
    contact_ls = ContactList.objects.get(id=id)

    if contact_ls in response.user.contactlist.all():
        context = {
            'contact_ls': contact_ls,
        }
        return render(response, 'contact_list.html', context)

    # TODO: verify this. Search about difference between this and redirect/httpredirect
    return render(response, 'registration/login.html')


@login_required(login_url=login_url)
def contact_detail(response, id):
    contact_det = Contact.objects.get(id=id)

    if response.user.contactlist.get(id=contact_det.contact_list.id):
        context = {
            'contact': contact_det,
        }
        return render(response, 'contact_item.html', context)

    # TODO: verify this. Search about difference between this and redirect/httpredirect
    return render(response, 'registration/login.html')


@login_required(login_url=login_url)
def create_list(response):
    form = CreateNewList()
    errors = []

    if response.method == 'POST':
        form = CreateNewList(response.POST)

        if form.is_valid():
            name_ = form.cleaned_data["name"]
            contact_list_ = ContactList(name=name_)
            try:
                contact_list_.save()
                response.user.contactlist.add(ls)

                return HttpResponseRedirect(f'groups/{contact_list_.id}')
            except Exception as e:
                print(e)
                errors.append('The name is already in use. Try another name.')

    context = {
        'form': form,
        'errors': errors,
    }

    return render(response, 'create_list.html', context)


@login_required(login_url=login_url)
def create_contact(response):
    form = CreateNewContact()
    errors = []

    if response.method == 'POST':
        form = CreateNewContact(response.POST)

        if form.is_valid():
            contact_list_ = form.cleaned_data["contact_list"]
            name_ = form.cleaned_data["name"]
            phone_number_ = form.cleaned_data["phone_number"]
            email_ = form.cleaned_data["email"]
            contact_ = Contact(contact_list=contact_list_, name=name_, phone_number=phone_number_, email=email_)
            try:
                contact_.save()
                send_email(contact=contact_, recipient=response.user.email)
                print('here')

                return HttpResponseRedirect(f'contacts/{contact_.id}')
            except Exception as e:
                print(e)
                errors.append('The contact seems to be already registered. Please verify the contact info.')

    context = {
        'form': form,
        'errors': errors,
    }

    return render(response, 'create_contact.html', context)
