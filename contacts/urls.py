from django.urls import path
from .views import home, contacts, contact_list, contact_detail, create_list, create_contact, contact_groups

# app_name = 'contacts'

urlpatterns = [
    path('', home, name="home"),
    path('groups/', contact_groups, name='contact_groups'),
    path('contacts/', contacts, name="contacts"),
    path('groups/<int:id>', contact_list, name="contact_list"),
    path('contacts/<int:id>', contact_detail, name="contact_detail"),
    path('create_list', create_list, name="create_list"),
    path('create_contact', create_contact, name="create_contact"),
]