from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class ContactList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="contactlist", null=True)
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class Contact(models.Model):
    contact_list = models.ForeignKey(ContactList, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, unique=True)
    email = models.EmailField(unique=True)
    phone_number = models.IntegerField(unique=True)

    def __str__(self):
        return self.name

