from django import forms
from .models import ContactList


class CreateNewList(forms.Form):
    name = forms.CharField(label="Name", max_length=20, required=True)


class CreateNewContact(forms.Form):
    contact_list = forms.ModelChoiceField(label="Contact List", queryset=ContactList.objects.all(), required=True)
    name = forms.CharField(label="Name", max_length=20, required=True)
    email = forms.EmailField(label="Email", required=True)
    phone_number = forms.IntegerField(label="Phone number", required=True)


# class SendEmailForm(forms.Form):
#     send_to = forms.EmailField(label="Send to:", max_length=50, required=True)
#     subject = forms.CharField(label="Subject", max_length=30, required=True)
#     message = forms.CharField(label="Message", max_length=500, widget=forms.Textarea)
