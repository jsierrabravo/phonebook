from django.contrib import admin
from .models import ContactList, Contact

# Register your models here.
admin.site.register(ContactList)
admin.site.register(Contact)
