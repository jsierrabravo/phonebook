from phonebook.celery import app
from django.core.mail import send_mail
from django.conf import settings


@app.task
def send_email(contact=None, recipient=None):
    try:
        print('error here')
        message = f"New contact added to {contact.contact_list}: " \
                  f"\nName: {contact.name} " \
                  f"\nPhone number: {contact.phone_number}" \
                  f"\nEmail address: {contact.email}"
        print('message', message)
        print(contact)
        print(recipient)
        send_mail(
            subject='A new contact was created',
            message=message,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[
                recipient
            ]
        )
        status = True

    except Exception as e:
        print('Exception:')
        print(e)
        status = False

    print('status:', status)
    # return status
