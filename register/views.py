from django.shortcuts import render, redirect
from .forms import RegisterForm


# Create your views here.
def register(response):
    form = RegisterForm()

    if response.method == 'POST':
        form = RegisterForm(response.POST)
        if form.is_valid():
            # TODO: create an exception to handle the view if it rises an error
            form.save()

        return redirect("/")

    context = {
        'form': form,
    }

    return render(response, 'register/register.html', context)
